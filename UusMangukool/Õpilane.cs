﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UusMangukool
{
    public class Õpilane : Inimene
    {
        // kuna me tegime muudatuse failist lugemise meetodis
        // muudame siin seda muutujat
        static string Filename = "õpilased";
        private static int _Start = 0;
        public static int Start { get => _Start;
            set => _Start = value; }

        
        // õpilaste "kollektsioon"
        public static IEnumerable<Õpilane> Õpilased
              //            => Inimene.Nimekiri.OfType<Õpilane>();
              => Inimene.Nimekiri.Where(x => x is Õpilane).Select(x => (Õpilane)x);
        // lisaks nimele ja isikukoodile on õpilastel veel ka klass, kus nad õpivad
        public string Klass { get; set; } // kus klassis õpib

        // õpilase konstruktor - vajalik, kuna Inimesel puudub parameetriteta konstruktor
        public Õpilane(string ik, string nimi) : base(ik, nimi) { }

        // staatiline konstruktor (kutsub välja meetodi, mis loeb nimekirja failist)
        static Õpilane() => LoeFailist();
        public static void Init() { } // => LoeFailist();
        // kuna failist lugemine piirdub nüüd ühe avalidsega
        // saab kasutada "lühendatud" meetodivormi
        static void LoeFailist()
        {
   
            
            {
                Filename.Readfile()
                    .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                    .ToList()
                    .ForEach(x => new Õpilane(x[0], x[1]).Klass = x[2])
                ;
            }
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}
        }
        // sama faili kirjutamisega ( aga me ei kasuta selles rakenduses seda)
        static void KirjutaFaili()
        =>
            File.WriteAllLines(Filename,
            Õpilased.Select(x => $"{x.IK}, {x.Nimi}, {x.Klass}"));

        // to string tehtud ka lühikese avaldisega
        public override string ToString() => $"{Klass} klassi-i õpilane {Nimi} (ik {IK})";


    }
}
