﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UusMangukool
{
    public class Täiskasvanu : Inimene
    {
        // kuna me tegime muudatuse failist lugemise meetodis
        // muudame siin seda muutujat
        static string Filename = "täiskasvanud";
        public static int Start = 0;
        // teen kohe tühja konstryuktori
        // kuna baasklassil PUUDUB parameetriteta konstruktor
        Täiskasvanu(string ik, string nimi) : base(ik, nimi) { }

        public static IEnumerable<Täiskasvanu> Täiskasvanud => Inimene.Nimekiri.OfType<Täiskasvanu>();
        public static IEnumerable<Täiskasvanu> Õpetajad => Inimene.Nimekiri.OfType<Täiskasvanu>().Where(x => x.Aine != "");

        public static IEnumerable<Täiskasvanu> Lapsevanemad => Inimene.Nimekiri.OfType<Täiskasvanu>().Where(x => x.KasLapsevanem);

        // see on Ainekood, mida see inimene õpetab
        string _Aine = ""; public string Aine => _Aine;
        public bool KasÕpetaja => _Aine != "";

        // see on klass, mida juhatab
        string _Klass = ""; public string Klass => _Klass;
        public bool KasJuhataja => _Klass != "";

        // siin on lapsed - kui neid ei ole, siis pole lapsevanem
        List<Õpilane> _Lapsed = new List<Õpilane>();
        public IEnumerable<Õpilane> Lapsed => _Lapsed.AsEnumerable();
        public bool KasLapsevanem => _Lapsed.Count > 0;

        // nüüd teeme selle failist lugemise asja

        static Täiskasvanu() // staatiline konstruktor kutsutakse enne teisi asju
        {
            LoeFailist();
        }

        public static void Init() { } // => LoeFailist();

        static void LoeFailist() // see kutsutakse välja staatilisest konstruktorist
        {
            // isikukood, nimi, aine, klass, lapsed,...
            bool kumb = false;
            if (kumb)
                Filename.ReadFile()  // stringimassiiv
                    .Select(x => (x + ",,,,").Split(',').Select(y => y.Trim()).ToArray())
                    .Select(x => new { Inimene = new Täiskasvanu(x[0], x[1]), Muud = x.Skip(2).ToArray() })
                    .ToList()
                    .ForEach(
                            x =>
                            {
                                x.Inimene._Aine = x.Muud[0];
                                x.Inimene._Klass = x.Muud[1];
                                x.Inimene._Lapsed = x.Muud.Skip(2).Where(y => y != "").Select(y => Inimene.ByIk(y)).OfType<Õpilane>().ToList();
                            });
            else
            {
                string[] loetudread = Filename.ReadFile();
                foreach (var loetudrida in loetudread)
                {
                    string[] osad = (loetudrida + ",,,,").Split(',');
                    var täis = new Täiskasvanu(osad[0].Trim(), osad[1].Trim());
                    täis._Aine = osad[2].Trim();
                    täis._Klass = osad[3].Trim();
                    for (int i = 4; i < osad.Length; i++)
                    {
                        if (osad[i].Trim() != "")
                        {
                            Õpilane laps = Inimene.ByIk(osad[i].Trim()) as Õpilane; ;
                            if (laps != null) täis._Lapsed.Add((Õpilane)laps);
                        }
                    }


                }
            }



        }
        public override string ToString()
        =>
            (KasÕpetaja ? 
                ((Õppeaine.ByKood(_Aine)?.Nimetus ?? _Aine) + " õpetaja " 
                    +
                    (KasJuhataja ? $" ja {_Klass} klassi juhataja " : "")
                ) 
                : "") 
            + Nimi;
    }
}
