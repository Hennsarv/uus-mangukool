﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UusMangukool
{
    class Klass
    {
        static string Filename = @"..\..\andmed\Klassid.txt";

        public static Dictionary<string, Klass> Klassid = new Dictionary<string, Klass>();
        string _Kood; public string Kood => _Kood;

        // siia tulevad ained, mida seal klassis õpetatakse
        // selle listis on ainete KOODID
        public List<string> Ained = new List<string>();

        // selle klassi õpilased on (loendatav asi) need kõigi õpilaste hulgast
        // kelle klass on just see siin
        public IEnumerable<Õpilane> Õpilased 
            => Õpilane.Õpilased.Where(x => x.Klass == this._Kood);

        Klass(string kood)
        {
            _Kood = kood;
            Klassid.Add(kood, this);
        }

        static Klass() => LoeFailist();

        //static void LoeFailist()
        //    => File.ReadAllLines(Filename)
        //        .Select(x => x.Split(',').Select(y => y.Trim()).ToList())
        //        .ToList()
        //        .ForEach(x => new Klass(x[0]).Ained = x.Skip(1).ToList())
        //    ;

            static void LoeFailist()
        {
            
                foreach (var loetudRida in Filename.Readfile())
            {
                string[] jupid = loetudRida.Split(',');
                // jupid[0] on klassi kood-nimi "1A"
                // jupid[1..ülejäänud] on õppeaine koodid
                Klass õ = new Klass(jupid[0].Trim());
                for (int i = 1; i < jupid.Length; i++) õ.Ained.Add(jupid[i].Trim());
            }
        }

        // veel on puudu faili kirjutamine
        // et võib tulevikus vaja minna
        static void KirjutaFaili()
            => File.WriteAllLines(Filename,
            Klassid.Values
                .Select(x => x.Kood + 
                        string.Join("",
                            x.Ained.Select(y => $", {y}")))
                );

        public static Klass ByCode(string kood)
            => Klassid.Keys.Contains(kood) ? Klassid[kood] : null;

        public override string ToString()
        { return $"Klass {Kood}";  }

    }
}
