﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UusMangukool
{
    public class Õppeaine
    {
        // kuna me tegime muudatuse failist lugemise meetodis
        // muudame siin seda muutujat
        static string filename = "õppeained";

        public static Dictionary<string, Õppeaine> Õppeained =
            new Dictionary<string, Õppeaine>();

        private string _Kood; public string Kood => _Kood;
        private string _Nimetus;
        public string Nimetus
        {
            get => _Nimetus;
            set       // kui on vaja muuta
            {
                if (value != "")
                {
                    if (value != _Nimetus)
                    {
                        _Nimetus = value;
                        KirjutaFaili();
                    }
                }
            }
        }

        // lisame õppeainetele staatilise konstruktori
        // see kutsutakse välja, kui esimest korda mõnda selle klassi meetodit 
        // käivitatakse (enne kõiki teisi)

        // staatilisel konstruktoril ei ole juurdepääsu ega parameetried
        static Õppeaine()
        {
            LoeFailist(); 
        }



        // konstruktori teeme privaatseks, sest uute õppeianete lisamine
        // ja failist laadimine on klassi sees
        private Õppeaine(string kood, string nimetus)
        {
            // konstruktor - iga aine luuake konstruktoris
            _Kood = kood; 
            _Nimetus = nimetus;
            // kõik ained lisatakse sinna dictionary
            Õppeained.Add(kood, this);
        }

        // lisame siia uue meetodi 
        // teeme loefailist privaatseks, kuna see käivitatakse
        // staatilisest konstruktorist
        private static void LoeFailist()
        {
            // loeme õppeained failist sisse
            // see meetod võiks olla õppeainete klassis staatilisena
            // aga ma teen ta praegu siinsamas


            string[] loetudread = filename.ReadFile();
            foreach (var rida in loetudread)
            {
                string[] osad = rida.Split(',');
                string koodx = osad[0].Trim();
                string nimetus = osad[1].Trim();
                if (!Õppeaine.Õppeained.Keys.Contains(koodx))
                {
                    new Õppeaine(koodx, nimetus);
                }
            }

        }

        private static void KirjutaFaili()
        => Õppeaine.Õppeained.Values.Select(x => $"{x._Kood}, {x._Nimetus}").WriteFile(filename);


        //{ // eemaldasin vana "iganenud" failikirjutamise meetodi
        //    List<string> kirjutatavadRead = new List<string>();
        //    foreach (var x in Õppeaine.Õppeained.Values)
        //    {
        //        string kirjutatavRida = $"{x._Kood}, {x._Nimetus}";
        //        kirjutatavadRead.Add(kirjutatavRida);
        //    }
        //    File.WriteAllLines(filename, kirjutatavadRead);


        //}
        // teeme ka faili kirjutamise privaatseks, kuna lisamisel aj kustutamisel
        // kirjutame niiehknii

        // tegin kaks meetodit-funkltsiooni
        // üks õppeaine lisamiseks teine kustutamiseks
        // mõlemad annavad avstuseks, kas õnnestus või ei
        public static bool LisaÕppeaine(string kood, string nimetus)
        {
            if (Õppeained.Keys.Contains(kood)) return false; // kood juba olemas
            new Õppeaine(kood, nimetus);

            // et ei ununeks salvestamine, teeme kohe
            KirjutaFaili();

            return true; // õppeaine lisatud
        }

        public static bool KustutaÕppeaine(string kood)
        {
            if (Õppeained.Keys.Contains(kood))
            {
                Õppeained.Remove(kood);

                // ka siin tuleb kohe krijutada faili
                // et ei ununeks
                KirjutaFaili();

                return true; // kustutatud
            }
            else return false; // sellist pole
        }

        // lisasime koodi järgi otsimise staatilise funktsiooni
        // mis vastuseks annab leitud õppeaine või nulli
        public static Õppeaine ByKood(string kood)
        {
            if (Õppeained.Keys.Contains(kood)) return Õppeained[kood];
            else return null;
        }


        public override string ToString()
        {
            return $"({_Kood}) - {_Nimetus}";
        }

    }
}
