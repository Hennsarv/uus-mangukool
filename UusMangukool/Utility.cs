﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UusMangukool
{
    public static class Utility
    {
        // kõigil klassidel on see failinimi ühtemoodi
        // paneme selle failinime alguse siia utiliidi klassi
        // et kui vaja muuta failide asukohta, hää ja lihtne
        static string _Path = @"c:\Andmed\";
        
        public static string Path { set => _Path = value;  }
        // igaks juhuks, ehk tuleb tulevikus seda failide asukohta
        // kuskilt ette anda (write-only property)

        // tegin samast KAKS varianti erinevad 1 suurtähe poolest
        // 1. kasutab if-lauset ja seepärast ka loogelisi sulge
        public static string[] Readfile(this string filename)
        {
            if (File.Exists(_Path + filename + ".txt"))
                return File.ReadAllLines(_Path + filename + ".txt");
            else
            //Console.WriteLine($"faili {filename} pole ollagi");
                return new string[0];
        }
        // 2. kasutab ? : avaldist ja et tegu ühe avaldisega, siis =>
        public static string[] ReadFile(this string filename)
            => File.Exists(_Path + filename + ".txt") 
            ? File.ReadAllLines(_Path + filename + ".txt") 
            : new string[0];

        public static void WriteFile(this IEnumerable<string> read, string filename)
            => File.WriteAllLines(_Path + filename + ".txt", read);
    }
}
