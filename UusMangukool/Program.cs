﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UusMangukool
{
    class Program
    {
        static void Main()
        {
            // muudan ära failide asukoha
            Utility.Path = @"..\Andmed\";
            // igaks juhuks veendun, et õpilased-õpetajad on mälus
            
            // testime, kuidas õpilaste failist lugemine õnnestub
            foreach (var x in Õpilane.Õpilased) Console.WriteLine(x);
            foreach (var x in Klass.Klassid) Console.WriteLine(x);

            Console.WriteLine("\nproovime inimesi\n");

            foreach(var x in Inimene.Nimekiri)
            {
                switch(x)
                {
                    case Õpilane õ:
                        // siia see, mis teha õpilasega
                        break;
                    case Täiskasvanu t:
                        // Täiskasvanuga
                        break;
                    default:
                        // muude inimestega
                        break;
                }


            }


            Console.WriteLine("\nlõpetasime inimeste proovimise\n");


            Console.WriteLine("\nkes mida õpib loetelu\n");
            // kes mida õpib
            foreach( var x in Õpilane.Õpilased)
            {
                // x on siin õpilane
                // kõik õpilased sõelutakse läbi
                // x.Klass on klass kus ta õpib
                Console.WriteLine($"{x.Nimi} õpib {x.Klass} klassis");

                Klass k = Klass.ByCode(x.Klass);
                // nüüd on muutujas k see Klass kui x õpib
                if(k != null)
                {
                    // siin saame selle klassi õppeainetega tegelda
                    Console.WriteLine("tema klassis õpetatakse selliseid aineid");
                    foreach (var a in k.Ained)
                    {
                        Console.WriteLine(Õppeaine.ByKood(a)?.Nimetus??
                            $"mingi tundmatu aine: {a}");
                    }
                }
                else Console.WriteLine("sellist klassi meie koolis ei ole");
            }
            Console.WriteLine("\nloetelu otsas\n");

                // testime, kuidas klasside failist lugemine õnnestub
                foreach (var x in Klass.Klassid)
            {
                Console.WriteLine(x.Value);
                Console.WriteLine("\tõpetatavad ained:");
                foreach (var y in x.Value.Ained)
                    Console.WriteLine(Õppeaine.ByKood(y)?.ToString() ?? $"tundmatu aine {y}");
                Console.WriteLine("\tklassis õpivad");
                foreach(var y in x.Value.Õpilased)
                    Console.WriteLine(y);
            }
            Console.WriteLine("Täiskasvanud:");
            foreach(var x in Täiskasvanu.Täiskasvanud)
            {
                Console.WriteLine(x);
                //if (x.KasÕpetaja) Console.WriteLine($"\t{x.Aine} õpetaja");
                //if (x.KasJuhataja) Console.WriteLine($"\t{x.Klass} klassi juhataja");
                if (x.KasLapsevanem)
                {
                    Console.Write("\tlapsevanem - lapsed: ");
                    foreach (var y in x.Lapsed) Console.Write($"{ y._Nimi} ");
                    Console.WriteLine();
                }  
            }

            Console.WriteLine("\nÕpetajad: ");


            foreach (var x in Täiskasvanu.Õpetajad) Console.WriteLine($"");

            TööÕppeainetega();

        }

        // ennemalt tehtud Maini nimetasin ümber 
        // seda saab nüüd mainist välja kutsuda
        static void TööÕppeainetega()
        {

            // selle koha peal, kus enne oli funktsioon
            // Õppeaine.LoeFailist();
            // aga kui am unustan alguses lugeda õppeained failist
            // mida siis teha?
            // appi tuleb staatiline konstruktor
            // võtame siit failist lugemise ära

            string kood = ""; // seda kasutame allpool
            string nimetus = ""; // et allpool mitmes kohas kasutada

            // siin on tüüpiline menüü-switch näide
            // lõpmatu tsükkel, milles küsitakse tegevus
            bool kasJätkan = true;
            while (kasJätkan)
            {
                Console.WriteLine("õppeainete lisamine (a), kustutamine (d), vaatamine (r), uuendamine (u), lõpetamine (x)");
                // ja siin switch, mille kaudu juhitakse
                switch ((Console.ReadLine()+"x").Substring(0, 1).ToLower())
                {
                    case "r":
                        foreach (var x in Õppeaine.Õppeained.Values)
                        {
                            Console.WriteLine(x);
                        }
                        break;
                    case "a":
                        Console.WriteLine("lisame uued õppeained");
                        kood = "*";
                        while (kood != "")
                        {
                            Console.Write("Anna kood: ");
                            kood = Console.ReadLine();
                            if (kood != "")
                            {
                                Console.Write("Anna nimetus: ");
                                nimetus = Console.ReadLine();
                                //Õppeaine u = new Õppeaine(kood, nimetus);
                                //Console.WriteLine("õppeaine loodud");

                                if (Õppeaine.LisaÕppeaine(kood, nimetus))
                                    Console.WriteLine("õppeaine loodud");
                                else
                                    Console.WriteLine("sellise koodiga juba on");
                            }
                        }
                        break;
                    case "d":
                        // kuidas teha kustutamine
                        Console.Write("anna kood: ");
                        kood = Console.ReadLine();
                        //if (Õppeaine.Õppeained.Keys.Contains(kood))
                        //{
                        //    Õppeaine.Õppeained.Remove(kood);
                        //    Console.WriteLine("lännu");
                        //}
                        //else
                        //{
                        //    Console.WriteLine("sellist õppeainet pole");
                        //}
                        if (Õppeaine.KustutaÕppeaine(kood))
                            Console.WriteLine("kustutatud");
                        else
                            Console.WriteLine("sellise koodiga õppeainet pole");
                        break;
                    case "u":
                        Console.Write("anna kood: ");
                        kood = Console.ReadLine().ToLower();
                        Console.Write("anna uus nimi: ");
                        nimetus = Console.ReadLine();
                        Õppeaine õ = Õppeaine.ByKood(kood);
                        if (õ != null) õ.Nimetus = nimetus;
                        else Console.WriteLine("sellise koodiga ei ole");
                        break;
                    case "x":
                        Console.WriteLine("headaega");
                        kasJätkan = false;
                        break;
                    default:
                        Console.WriteLine("ma küll aru ei saanud, mida teha");
                        break;
                }
            }
            // Õppeaine.KirjutaFaili();
            // aga kui ma lõpus unustan faili kirjutada, mis siis juhtub
            // staatilist "destructorit" ei ole olemas
            // lahenduse leiame õppeaine lisamise meetodist - VT!

        }
    }
}
