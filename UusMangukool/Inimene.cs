﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UusMangukool
{
    public class Inimene
    {
        public static Dictionary<string, Inimene> Inimesed
            = new Dictionary<string, Inimene>()
            ;


        public static IEnumerable<Inimene> Nimekiri => Inimesed.Values; 



        private string _IK; public string IK => _IK;
        public string _Nimi; public string Nimi => _Nimi;

        public static Inimene ByIk(string ik) => Inimesed.Keys.Contains(ik) ? Inimesed[ik] : null;

        public Inimene(string ik, string nimi)
        {
            _IK = ik;
            _Nimi = nimi;
            try
            {
                Inimesed.Add(ik, this);
            }
            catch
            {

                throw new Exception($"isikukood {ik} on topelt");
            }
        }

        public override string ToString() => $"inimene {_Nimi} (ik: {_IK})";
        
    }
}
