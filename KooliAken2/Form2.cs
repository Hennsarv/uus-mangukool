﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UusMangukool;

namespace KooliAken2
{
    public partial class Form2 : Form
    {
        internal Täiskasvanu Kes;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.Text = "Siin töötab: " + Kes.ToString();
        }

        private void maksudToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var x = Õpilane.Õpilased.ToList();
            this.dataGridView1.DataSource = x;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Õpilane(this.textBox2.Text, this.textBox1.Text) { Klass = this.textBox3.Text };
        }
    }
}
