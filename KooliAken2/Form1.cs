﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UusMangukool;

namespace KooliAken2
{
    public partial class Form1 : Form
    {
        Inimene kes = null;

        static List<Form1> Aknad = new List<Form1>();
        public Form1()
        {
            Aknad.Add(this);
            InitializeComponent();
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            if (this.button1.Text == "Login")
            {

                kes = Inimene.ByIk(textBox1.Text);
                if (kes == null)
                {
                    this.label2.Text = "ei ei ei sind tunne ma";
                }
                else

                {
                    this.button1.Text = "Logoff";
                    this.button2.Visible = true;
                    if (kes is Õpilane kesÕ)
                    {
                        //Õpilane kesÕ = (Õpilane)kes; // selle asemel ifis uus muutuja
                        this.label2.Text = $"Tere õpilane {kesÕ.Nimi} (klassist {kesÕ.Klass})";
                    }
                    else if (kes is Täiskasvanu kesT)
                    {
                        this.label2.Text = $"Tere ";
                        if (kesT.KasÕpetaja)
                        {
                            this.label2.Text += $"{kesT.Aine} õpetaja ";
                            if (kesT.KasJuhataja)
                                this.label2.Text += $"ja {kesT.Klass} klassi juhataja ";

                        }
                        if (kesT.KasLapsevanem)
                            this.label2.Text += "(ja sa oled ka lapsevanem) ";
                        this.label2.Text += kesT.Nimi + "!";
                    }
                }
            }
            else
            {
                this.label2.Text = $"Headaega {kes.Nimi}";
                this.button1.Text = "Login";
                this.button2.Visible = false;
                kes = null;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            (new Form1()).Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
     

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (kes is Õpilane kesÕ) (new Form3() { Kes = kesÕ }).Show();
            else (new Form2() { Kes = (Täiskasvanu)kes }).Show();

        }
    }
}