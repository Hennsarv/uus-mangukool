﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UusMangukool;
using System.IO;

namespace KooliWeeb
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Õpilane.Init();

            this.Label2.Text = Path.GetFullPath(".");

            var x = Õpilane.Õpilased.ToList();
            this.GridView1.DataSource = x;
            GridView1.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            this.Label1.Text = "Tere " + this.TextBox1.Text + "!";
        }
    }
}